package pl.codementors;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class ResultResource {

    //object type File - to be constructed in main method
    private File file;

    ResultResource(File file){
        this.file = file;
    }

    public synchronized void save(String line){
        try (FileWriter fw = new FileWriter(file, true)) {
            fw.write(line);
            fw.write("\n");
        } catch (IOException ex) {
            System.err.println(ex);
        }

    }
}

