package pl.codementors;

import java.io.File;

public class PrimeMain {

    static public void main(String[] args){

        ResultResource resources = new ResultResource(new File("file.txt"));
        PrimeWorker worker1 = new PrimeWorker(3, resources);

        new Thread(worker1).start();
    }

    static boolean isPrime(int number){

        if(number < 2){
            return false;
        }else{
            for(int i=2; i <= number/2; i++){
                if(number % i == 0){
                    System.out.println(number);
                    return false;
                }
            }
            System.out.println(number);
            return true;
        }
    }
}


// z 2 param (plik, true) - true dopisuje a nie pozwala nadpisywac ws pliku