package pl.codementors;

import java.io.File;

public class PrimeWorker implements Runnable{

    private ResultResource resource;
    private int number;

    PrimeWorker(int number, ResultResource resource){
        this.number = number;
        this.resource = resource;
    }

    @Override
    public void run() {
        boolean isTrue = PrimeMain.isPrime(number);
        resource.save(String.valueOf(number) + " " + isTrue);
    }
}
